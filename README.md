# bit-brigade

bit-brigade is a [Brigade](https://github.com/Azure/brigade) Project that clones from BitBucket

## Prerequisites

1. Have a running [Kubernetes](https://kubernetes.io/docs/setup/) environment
2. Setup [Helm](https://github.com/kubernetes/helm)

## Install

### Set up Brigade

Follow the [quick-start guide](https://github.com/Azure/brigade#quickstart):

Install Brigade into your Kubernetes cluster is to install it using Helm.

```bash
$ helm repo add brigade https://azure.github.io/brigade
$ helm install -n brigade brigade/brigade
```

To manually run Brigade Projects the **brig** binary is required. Follow the
[Developers Guide](https://github.com/Azure/brigade/blob/master/docs/topics/developers.md)
to build the binary. Assuming Brigade is cloned and prerequisites met, simply run:
```bash
$ make brig
```
Test **brig** with `brig version`

### Install bit-brigade

Clone bit-brigade and change directory
```bash
$ git clone https://bitbucket.org/lukepatrick/bit-brigade.git
$ cd bit-brigade
```
Helm install bit-brigade
> note the name and namespace can be customized
```bash
$ helm install --name bit-brigade brigade/brigade-project -f bit-brigade.yaml
```


## Usage

Manually run the project. The project name is the same as the project value in
the *bit-brigade.yaml*
```bash
$ brig run lukepatrick/bit-brigade
```

If *brigade.js* is customized then running the Project will need to have source *brigade.js* overriden

Run with override:
```bash
$ brig run lukepatrick/bit-brigade -f brigade.js
```

### Webhooks


## Contribute

PRs accepted.

## License

MIT