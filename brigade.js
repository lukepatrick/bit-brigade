const CONTAINER = "alpine:3.4"

const { events, Job } = require("brigadier")

events.on("exec", test)
events.on("push", test)

function test(e, p) {
  // env info
  console.log("==> Project " + p.name + " clones the repo at " + p.repo.cloneURL)
  console.log("==> Event " + e.type + " caused by " + e.provider)

  // create job with name and container image to use
  var simple_job = new Job("my-simple-job", CONTAINER)


  //set up tasks
  simple_job.tasks = ['ls',
                      'pwd',
                      'echo Hello BitBucket']



  //console.log("==> Set up tasks, env, Job ")
  //debug only
  //console.log(simple_job)

  console.log("==> Running Simple Job")

  // run Job, get Promise and print results
  simple_job.run().then( resultSimpleJob => {
    //debug only
    console.log("==> Simple Job Results")
    console.log(resultSimpleJob.toString())
    console.log("==> Simple Job Done")
  })

  

}